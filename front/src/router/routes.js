
const routes = [
  {
    path: '/',
    component: () => import('layouts/MainLayout.vue'),
    children: [
      { path: '', component: () => import('pages/ListarPost.vue') },
      { path: '/post/my', component: () => import('pages/MisPost.vue') },
      { path: '/post/crear', component: () => import('pages/CrearPost.vue') },
      { path: '/post/:id', component: () => import('pages/MostrarPost.vue') }
    ]
  },

  // Always leave this as last one,
  // but you can also remove it
  {
    path: '/:catchAll(.*)*',
    component: () => import('pages/ErrorNotFound.vue')
  }
]

export default routes
